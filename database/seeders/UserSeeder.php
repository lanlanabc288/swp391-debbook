<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = array(
            'user_id' => '1',
            'user_name' => 'huanxieu69',
            'email' => 'lanlanabc288@gmail.com',
            'password' => '$2y$10$rGHtKyLc7N87gfXn0yUjxuQNsRpV95NbSO5/wtq09TbOLMB8MQlx6',
            'status' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        );
        User::insert($user);
    }
}
