<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\forgotPasswordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login.index');
});

Route::get('login', function () {
    return view('login.index');
})->name('default');

Route::get('register', function () {
    return view('register.register');
});

Route::get('forgot-password', function () {
    return view('forgotpassword.forgot');
});

Route::get('log-out', function () {
    session([
        'userId' => null,
        'userName' => null
    ]);
    return view('login.logout');
});

Route::post('login', [LoginController::class, 'index'])->name('ajax.login');

Route::post('welcome', [HomeController::class, 'index']);

Route::post('dashboard', [DashboardController::class, 'index'])->name('ajax.dashboard');

Route::post('register-new', [RegisterController::class, 'index'])->name('ajax.register');

Route::get('/active-account/{token}', [RegisterController::class, 'active']);

Route::post('forgot-password', [forgotPasswordController::class, 'index'])->name('ajax.forgot');

Route::post('forgot-send-mail', [forgotPasswordController::class, 'sendMail'])->name('ajax.forgot-confirm-mail');

Route::get('/change-password/{token}', [forgotPasswordController::class, 'changeFromToken']);

Route::post('change', [forgotPasswordController::class, 'change'])->name('ajax.change');





