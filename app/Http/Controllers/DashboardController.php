<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
        if (session('userId') == null) {
            return view('login.index');
        } else {
            return view('dashboard.debtor');
        }
    }
}
