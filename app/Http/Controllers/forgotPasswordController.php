<?php

namespace App\Http\Controllers;

use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Notifications\InvoicePaid;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class forgotPasswordController extends Controller
{
    public function index(Request $request)
    {
        $email = $request->email;
        $data['message'] = '';
        $user = User::getUserInfo($email);
        if ($user == null) {
            $data['message'] = 'This account does not exist';
        } else {
            $data['email'] = $email;
            $data['token'] = Str::uuid();

            // Insert new token
            Token::insert([
                'token' => $data['token'],
                'email' => $email,
                'expiration_date' => Carbon::now()->addHour(24)
            ]);
        }

        return response()->json($data);
    }

    public function sendMail(Request $request)
    {
        $data['email'] = $request->email;
        $data['token'] = $request->token;
        $data['subject'] = '[Notice] - Change password Debbook account!';
        $data['view'] = 'forgotpassword.mail';

        //sent mail notification
        $user = new User();
        $user->email = $data['email'];
        $user->notify(new InvoicePaid($data));
    }

    public function changeFromToken(Request $request)
    {
        $token = $request->token;
        $email = json_decode(Token::select()->where('token', $token)->first())->email;
        return view('forgotpassword.newPassword', ['email' => $email]);
    }

    public function change(Request $request)
    {
        dd($request->all());
        $token = $request->token;
        $email = json_decode(Token::select()->where('token', $token)->first())->email;
        return view('forgotpassword.newPassword', ['email' => $email]);
    }
}
