<?php

namespace App\Http\Controllers;

use App\Models\Token;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Notifications\InvoicePaid;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
        $user_id = Str::uuid();
        $user_name = $request->userName;
        $email = $request->email;
        $password = $request->password;
        $data['message'] = '';
        $data['email'] = $email;

        // check exist email
        $check = count(User::select()->where('email', $email)->get());
        if ($check > 0) {
            $data['message'] = 'This email is already registered, please choose another email';
        } else {
            //Insert new user
            User::insert([
                'user_id' => $user_id,
                'user_name' => $user_name,
                'email' => $email,
                'password' => Hash::make($password),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
        $data['token'] = Str::uuid();
        // Insert new token
        Token::insert([
            'token' => $data['token'],
            'email' => $email,
            'expiration_date' => Carbon::now()->addHour(24)
        ]);

        return response()->json($data);
    }

    public function sendConfirmMail(Request $request)
    {
        $data['email'] = $request->email;
        $data['token'] = $request->token;
        $data['subject'] = '[Notice] - Activate Debbook account!';
        $data['view'] = 'register.mail';

        //sent mail notification
        $user = new User();
        $user->email = $data['email'];
        $user->notify(new InvoicePaid($data));
    }

    public function active(Request $request)
    {
        $token = $request->token;
        $email = json_decode(Token::select()->where('token', $token)->first())->email;

        DB::table('users')->where('email', $email)->update([
            'status' => 1,
        ]);

        return view('login.done');
    }
}
