<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    public function index(Request $request)
    {
        $email = $request->email;
        $data['message'] = '';

        $user = User::getUserInfo($email);
        if ($user != null) {
            $hashedPassword = $user[0]->password;

            if (!password_verify($request->password, $hashedPassword)) {
                $data['message'] = 'Your account or password is wrong Please try again';
            } else {
                if ($user[0]->status == 0) {
                    $data['message'] = 'Account was not active';
                } else {
                    session([
                        'userId' => $user[0]->user_id,
                        'userName' => $user[0]->user_name
                    ]);
                }
            }
        } else {
            $data['message'] = 'Your account or password is wrong Please try again';
        }

        return response()->json($data);
    }
}
