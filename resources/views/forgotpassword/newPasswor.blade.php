<!DOCTYPE html>
<html lang="en">
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Change password</title>
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script> --}}
    <style>
        body,
        html {
            height: 100%;
            font-family: 'Courier New', Courier, monospace;
            background-color: #f8f8f8;
        }

        form {
            height: 80%;
        }

        h1 {
            font-weight: bold;
            color: #222
        }

        hr {
            width: 100px;
            border-top: 2px solid;
            color: blue;
            align-items: center;
        }

        .card {
            width: 60%;
            margin-left: 20%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .content {
            background-color: rgb(192, 243, 192);
            color: rgb(59, 97, 59);
            text-align: center;
            border-radius: 10px;
            padding: 20px;
            font-size: 18px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .form-control {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .container-submit-button {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            padding-top: 20px;

        }

        .button-form-btn {
            font-family: Montserrat-Bold;
            font-size: 18px;
            line-height: 1.5;
            color: #fff;
            text-transform: uppercase;
            border: none;
            width: 100%;
            height: 50px;
            border-radius: 10px;
            background: #6dabe4;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0 25px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            -moz-transition: all 0.4s;
            transition: all 0.4s;
        }

        .button-form-btn:hover {
            background: #333333;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

</head>

<body>
    <section class="vh-150 gradient-custom">
        <div class="container py-5 h-100">
            <div class="row justify-content-center align-items-center h-100">
                <div class="col-12 col-lg-9 col-xl-7">
                    <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
                        <div class="card-body p-4 ">
                            <h1 class="mb-4 pb-2 pb-md-0 " style="text-align: center;">NEW PASSWORD</h1>

                            <div class="content">
                                Please create a new password that you don't use on any other site
                            </div>
                            <input type="text" value="{{ $email }}" id="newPWmail" hidden>
                            <form action="" method="">
                                <div class="row" style="padding: 20px 0px;">
                                    <div class="form-outline">
                                        <input type="password" id="newPassword" placeholder="Enter your new password"
                                            class="form-control form-control-lg" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-outline">
                                        <input type="password" id="confirmNew" placeholder="Confirm your password"
                                            class="form-control form-control-lg" />
                                    </div>
                                </div>
                            </form>
                            <div class="container-submit-button mt-2 pt-2">
                                <button class="button-form-btn" id="confirm-change">
                                    Change
                                </button>
                                <a href="/" style="font-family:Poppins ;margin-top: 20px">Back to login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form id="change" method="POST" action="{{ route('ajax.change') }}"></form>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="js/forgot.js"></script>
</body>

</html>
