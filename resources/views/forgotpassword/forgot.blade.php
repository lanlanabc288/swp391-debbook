<html lang="vi">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Forgot password</title>
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        body,
        html {
            height: 100%;
            font-family: 'Courier New', Courier, monospace;
            background-color: #f8f8f8;
        }

        form {
            height: 80%;
        }

        .card {
            width: 50%;
            margin-left: 25%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        h1 {
            font-weight: bold;
            color: #222;
            text-align: center;
        }

        hr {
            width: 100px;
            border-top: 2px solid;
            color: blue;

        }

        img {
            margin-left: 60px;
            height: 80%;
            width: 80%;
        }

        .form-label {
            font-size: 20px;
            font-family: Poppins;
        }

        .content {
            background-color: rgb(192, 243, 192);
            color: rgb(59, 97, 59);
            border-radius: 10px;
            text-align: center;
            font-size: 24px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            background-color: rgb(192, 243, 192);
            color: rgb(59, 97, 59);
            border-radius: 10px;
            text-align: center;
            font-size: 24px;
        }

        .form-control {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .container-submit-button {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            padding-top: 20px;

        }

        .button-form-btn {
            font-family: Montserrat-Bold;
            font-size: 18px;
            line-height: 1.5;
            color: #fff;
            text-transform: uppercase;
            border: none;
            width: 100%;
            height: 50px;
            border-radius: 10px;
            background: #6dabe4;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0 25px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            -moz-transition: all 0.4s;
            transition: all 0.4s;
        }

        .button-form-btn:hover {
            background: #333333;
        }

        /* ajax loading css */
        .modal {
            display: none;
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(255, 255, 255, .8) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat;
        }

        body.loading .modal {
            overflow: hidden;
        }

        body.loading .modal {
            display: block;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    <section class="vh-150 gradient-custom">
        <div class="container py-5 h-100">
            <div class="row justify-content-center align-items-center h-100">
                {{-- ajax loading --}}
                <div class="modal"></div>
                {{--  --}}
                <div class="col-12 col-lg-9 col-xl-7">
                    <div class="card shadow-2-strong card-registration" style="border-radius: 15px;">
                        <div class="card-body p-4">
                            <h1 class="mb-4 pb-2 pb-md-0">FORGOT PASSWORD</h1>
                            <div style="font-size: 20px;font-weight: bold;margin-left: 18px;color: green;">
                                Enter your email address</div>
                            <div class="message-error" style="color: red;margin-left: 32px;font-weight: bold">
                                
                            </div>
                            <div class="message-sucess" style="color: green;font-weight: bold;text-align: center">
                                
                            </div>
                            <form action="" method="">
                                <div class="row" style="padding: 20px 0;">
                                    <div class="form-outline">
                                        <input type="email" id="emailAddress" placeholder="Enter your email"
                                            class="form-control form-control-lg" />
                                    </div>

                                </div>
                            </form>
                            <div class="container-submit-button pt-2">
                                <button class="button-form-btn" id="continue" style="margin-top: -25px;">
                                    Continue
                                </button>
                                <a href="/"
                                    style="font-family:Poppins;margin-top: 20px;text-decoration: none;color: #6dabe4;">Back
                                    to login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form id="forgot-form" method="POST" action="{{ route('ajax.forgot') }}"></form>
    <form id="confirm-mail" method="POST" action="{{ route('ajax.forgot-confirm-mail') }}"></form>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="js/forgot.js"></script>
</body>

</html>
