<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="images/login.png" type="image/icon type">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Debbok</title>
</head>

<body>
    <H1>Register form</H1>

    <table>
        <tr>
            <td>User Name</td>
            <td><input type="text" id="userName"></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" id="email"></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" id="password"></td>
        </tr>
        <tr>
            <td>Confirm password</td>
            <td><input type="password" id="confirm-password"></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button id="submit">
                    Submit
                </button>
            </td>
        </tr>
    </table>

    <a href="{{ url('/') }}">Back to login</a>
    <form action="" id="register" method="post" url-register="{{ route('ajax.register') }}"></form>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="js/register.js"></script>
</body>

</html>
