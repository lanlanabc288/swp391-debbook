<html lang="vi">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Register</title>
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        body,
        html {
            height: 100%;
            font-family: 'Courier New', Courier, monospace;
            background-color: #f8f8f8;
        }

        form {
            height: 80%;
        }

        h1 {
            font-weight: bold;
            color: #222
        }

        hr {
            width: 100px;
            border-top: 2px solid;
            color: blue;

        }

        img {
            margin-left: 60px;
            height: 80%;
            width: 80%;
        }

        .form-label {
            font-size: 20px;
            font-family: Poppins;
        }

        .card-registration .select-input.form-control[readonly]:not([disabled]) {
            font-size: 1rem;
            line-height: 2.15;
            padding-left: .75em;
            padding-right: .75em;
        }

        .card-registration .select-arrow {
            top: 13px;
        }

        .container-submit-button {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            padding-top: 20px;
        }

        .button-form-btn {
            font-family: Montserrat-Bold;
            font-size: 18px;
            line-height: 1.5;
            color: #fff;
            text-transform: uppercase;
            border: none;
            width: 100%;
            height: 50px;
            border-radius: 10px;
            background: #6dabe4;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0 25px;

            -webkit-transition: all 0.4s;
            -o-transition: all 0.4s;
            -moz-transition: all 0.4s;
            transition: all 0.4s;
        }

        .button-form-btn:hover {
            background: #333333;
        }
        /* ajax loading css */
        .modal {
            display: none;
            position: fixed;
            z-index: 1000;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(255, 255, 255, .8) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat;
        }

        body.loading .modal {
            overflow: hidden;
        }

        body.loading .modal {
            display: block;
        }
        /*  */
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    <section class="vh-150 gradient-custom">
        <div class="container py-5 h-100">
            {{-- ajax loading --}}
            <div class="modal"></div>
            {{--  --}}
            <div class="row justify-content-center align-items-center h-100">
                <div class="col-12 col-lg-9 col-xl-7">
                    <div class="card shadow-2-strong card-registration"
                        style="border-radius: 15px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                        <div class="card-body p-4 p-md-5">
                            <h1 class="mb-4 pb-2 pb-md-0 mb-md-5">REGISTRATION
                                <hr>
                            </h1>
                            <img src="images/img-01.png" alt="Registration icon">
                            <form action="" method="">
                                <div class="row message-error"
                                    style="margin-left: 10px;margin-bottom: 5px;color: red;font-size: 20px;font-weight: bold">
                                </div>
                                <div class="row message-success"
                                    style="margin-left: 10px;margin-bottom: 5px;color: green;font-size: 20px;font-weight: bold">
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-4 pb-2">
                                        <div class="form-outline">
                                            <i class="fa fa-user icon"></i>
                                            <label class="form-label" for="UserName">Username</label>
                                            <input type="text" id="UserName" placeholder="Enter your username"
                                                class="form-control form-control-lg" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4 pb-2">
                                        <div class="form-outline">
                                            <i class="fa fa-envelope icon"></i>
                                            <label class="form-label" for="emailAddress">Email</label>
                                            <input type="email" id="email" placeholder="Enter your email"
                                                class="form-control form-control-lg" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-4">

                                        <div class="form-outline">
                                            <i class="fa fa-key icon"></i>
                                            <label class="form-label" for="Password">Password</label>
                                            <input type="password" id="password" placeholder="Enter your password"
                                                class="form-control form-control-lg" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">

                                        <div class="form-outline">
                                            <i class="fa fa-key icon"></i>
                                            <label class="form-label" for="Repass">Repeat password</label>
                                            <input type="password" id="confirm-password"
                                                placeholder="Confirm your password"
                                                class="form-control form-control-lg" />
                                        </div>

                                    </div>
                                </div>
                                <div class="container-submit-button mt-1 pt-2">
                                    <button class="button-form-btn" id="submit">
                                        Register
                                    </button>
                                    <a href="/"
                                        style="font-family:Poppins;margin-top: 20px;text-decoration: none;font-size: 20px">Back
                                        to login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form action="" id="register" method="post" url-register="{{ route('ajax.register') }}"></form>
    <form action="" id="notify-register" method="post" url-register="{{ route('ajax.notify-register') }}"></form>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="js/register.js"></script>
</body>

</html>
