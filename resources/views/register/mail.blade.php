<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        @media only screen and (max-width: 600px) {
            body hr {
                width: 90% !important;
                float: left;
                border: 1px solid;
            }
        }

        body hr {
            width: 41%;
            float: left;
            border: 1px solid;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="header">
            <ul style="list-style-type: none">
                <li style="border-top: 2px solid;max-width: 100%; width: 770px; padding:10px 0"> </li>
                <li>
                    <div>This email is automatically sent to the email address you entered.</div>
                </li>
                <br>
                <li style="border-top: 2px solid;max-width: 100%; width: 770px">
                </li>
        </div>
        <div class="body">
            <ul style="list-style-type: none">
                <li>
                    <div>
                        Click the URL below to active your account. </div>
                </li>
                <br>
                <li> <a href="debbook.com/active-account/{{ $token }}">
                        debbook.com/active-account/{{ $token }}</a>
                </li>
                <li>
                    <div style="font-size: 12px">
                        (The expiry date of this URL is 24 hours after you receive the email.)
                    </div>
                </li> <br><br><br>
                <li style="border-top: 2px solid;max-width: 100%; width: 770px; padding:10px 0"> </li>
                <li> Notes </li> <br>
                <li style="border-top: 2px solid;max-width: 100%; width: 770px; padding:10px 0"> </li>
                <li> ・This e-mail is a send-only e-mail address, so you cannot contact us by replying.
                </li>
                <li> ・If you do not recognize this email, please delete this email.
                </li>
                <li> ・Please refrain from reprinting or using this email without permission.
                </li>
                <li> ・If this token expires, please use the forgot password feature. </li>
                <br>
                <li style="border-top: 2px solid;max-width: 100%; width: 770px; padding:10px 0">
                </li>
            </ul>
        </div>
    </div>
</body>

</html>
