<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/login.css">
    <link rel="icon" href="images/login.png" type="image/icon type">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Login</title>
</head>

<body>
    <div class="main">
        <div class="container">
            {{-- ajax loading --}}
            <div class="modal"></div>
            {{--  --}}
            <div class="signin-content">
                <div class="signin-image">
                    <a href="{{ url('/') }}"><img src="images/login.png" alt="image"></a>
                    <a href="{{ url('register') }}" class="create-acc">Do not have an account? Create a new account</a>
                </div>

                <div class="signin-form">
                    <h2 class="form-title">SIGN UP TO DEBBOOK</h2>

                    <form action="{{ url('login') }}" id="login-form" method="post"
                        url-login="{{ route('ajax.login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="username"></label>
                            <input type="text" name="username" id="username" placeholder="Your email" />
                        </div>
                        <div class="form-group">
                            <label for="password"></label>
                            <input type="password" name="password" id="password" placeholder="Password" />
                        </div>
                        <!-- Error message -->
                        <div class="message-error" style="font-family: system-ui;font-weight: bold;">
                        </div>
                        <input type="button" class="form-submit" value="LOG IN" id="login" />
                        <a href="forgot-password" class="forgot-pass">Forgot password</a>
                    </form>
                </div>
            </div>
        </div>
    </div>  
    <form id="dashboard" method="POST" action="{{ route('ajax.dashboard') }}">
        {{ csrf_field() }}
        {{-- <input id="mode" name="mode" value="" hidden> --}}
    </form>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="js/login.js"></script>
    <script type="text/javascript">
        function checkValidate(text) {
            if(text == null || text == ""){
                return (
                    <div aria-live="polite" aria-atomic="true" style="position: relative; min-height: 200px;">
  <div class="toast" style="position: absolute; top: 0; right: 0;">
    <div class="toast-header">
      <img src="..." class="rounded mr-2" alt="...">
      <strong class="mr-auto">Notifications</strong>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="toast-body">
      The textbox can not be empty;
    </div>
  </div>
</div>
                )
            }
        }
    </script>
</body>

</html>
