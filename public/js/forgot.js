$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    $body = $("body");
    $body.addClass("loading");
    $body.removeClass("loading");

    $("#continue").click(function (e) {
        e.preventDefault();
        $('.message-error').empty();
        $(".message-sucess").empty();

        showAjaxLoading();
        var email = $("#emailAddress").val();
        var url = $("#forgot-form").attr('action');

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                email: email,
            },
            success: function (data) {
                hideAjaxLoading();
                if (data.message == '') {
                    $(".message-sucess").empty();
                    $(".message-sucess").append('A request to change your password has been sent to your email');
                    sendMail(data);
                }
                else {
                    // This account does not exist
                    $(".message-error").empty();
                    $(".message-error").append(data.message);
                }
            }
        });
        hideAjaxLoading();
    });

    $("#confirm-change").click(function (e) {
        alert('test');
        if ($("#newPassword").val() != $("#confirmNew").val()) {
            return;
        }
        var url = $("#change").attr('action');

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                email: $("#newPWmail").val(),
                password: $("#newPassword").val(),
            },
            success: function (data) {
                hideAjaxLoading();
                if (data.message == '') {
                    $(".message-sucess").empty();
                    $(".message-sucess").append('A request to change your password has been sent to your email');
                }
            }
        });
        hideAjaxLoading();
    })


    function sendMail(data) {
        var url = $("#confirm-mail").attr('action');

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                if (data.message == '') {
                    sentMailConfirm(data);
                    $(".message-success").empty();
                    $(".message-success").append('Successful registration, Please check your email to activate your account!');
                }
                else {
                    $(".message-error").empty();
                    $(".message-error").append(data.message);
                }
            }
        });
    }

    function showAjaxLoading() {
        $body.addClass("loading");
    }

    function hideAjaxLoading() {
        setTimeout(() => {
            $body.removeClass("loading");
        }, 250);
    }
})
