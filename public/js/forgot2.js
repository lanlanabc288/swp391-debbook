$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    $("#confirm-change").click(function (e) {
        if ($("#newPassword").val() != $("#confirmNew").val()) {
            return;
        }
        var url = $("#change").attr('action');

        $.ajax({
            type: 'POST',
            url: url,
            data: {
                email: $("#newPWmail").val(),
                password: $("#newPassword").val(),
            },
            success: function (data) {
                hideAjaxLoading();
                if (data.message == '') {
                    $(".message-sucess").empty();
                    $(".message-sucess").append(
                        'A request to change your password has been sent to your email');
                }
            }
        });
        hideAjaxLoading();
    });

    function showAjaxLoading() {
        $body.addClass("loading");
    }

    function hideAjaxLoading() {
        setTimeout(() => {
            $body.removeClass("loading");
        }, 250);
    }
})