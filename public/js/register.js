$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {

    $body = $("body");
    $body.addClass("loading");
    $body.removeClass("loading");

    $('#submit').click(function (e) {
        e.preventDefault();
        $(".message-error").empty();
        $(".message-success").empty();

        showAjaxLoading();
        var userName = $('#userName').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var confirmPassword = $('#confirm-password').val();
        var url = $("#register").attr('url-register');

        hideAjaxLoading();
        if (validateFormRegister(userName, email, password, confirmPassword)) {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    userName: userName,
                    email: email,
                    password: password,
                    confirmPassword: confirmPassword
                },
                success: function (data) {
                    if (data.message == '') {
                        sentMailConfirm(data);
                        $(".message-success").empty();
                        $(".message-success").append('Successful registration, Please check your email to activate your account!');
                    }
                    else {
                        $(".message-error").empty();
                        $(".message-error").append(data.message);
                    }
                }
            });
        } else {
            hideAjaxLoading();
        }
    })

    function validateFormRegister(userName, email, password, confirmPassword) {
        if (userName == '' || email == '' || password == '' || confirmPassword == '') {
            $(".message-error").empty();
            $(".message-error").append('Please fill in all the inputs below');
            return false;
        }

        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        } else {
            $(".message-error").append('Please enter your email to register');
            return false;
        }

        if (password != confirmPassword) {
            $(".message-error").empty();
            $(".message-error").append('Please enter the same password and confirm password');
            return false;
        }

        if (password.length < 8) {
            $(".message-error").empty();
            $(".message-error").append('Please enter password greater than or equal to 8');
            return false;
        }

        return true;
    }

    function sentMailConfirm(data) {
        var url = $("#notify-register").attr('url-register');

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                if (data.message == '') {
                    sentMailConfirm(data);
                    $(".message-success").empty();
                    $(".message-success").append('Successful registration, Please check your email to activate your account!');
                }
                else {
                    $(".message-error").empty();
                    $(".message-error").append(data.message);
                }
            }
        });
    }

    function showAjaxLoading() {
        $body.addClass("loading");
    }

    function hideAjaxLoading() {
        setTimeout(() => {
            $body.removeClass("loading");
        }, 250);
    }

});
