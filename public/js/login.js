$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    $body = $("body");
    $body.addClass("loading");
    $body.removeClass("loading");

    $("#login").click(function (e) {
        e.preventDefault();
        $('.message-error').empty();

        showAjaxLoading();
        var email = $("input[name=username]").val();
        var password = $("input[name=password]").val();
        var url = $("#login-form").attr('url-login');

        if (validateInput()) {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    email: email,
                    password: password,
                },
                success: function (data) {
                    hideAjaxLoading();
                    if (data.message == '') {
                        $("#dashboard").submit();
                    }
                    else {
                        $(".message-error").empty();
                        $(".message-error").append(data.message);
                    }
                }
            });
        } else {
            hideAjaxLoading();
        }
    });

    function validateInput() {
        $(".message-error").empty();

        var email = $("input[name=username]").val();
        var password = $("input[name=password]").val();

        if (email == '' && password == '') {
            $(".message-error").append('Email and Password cannot be blank');
            return false;;
        }

        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        } else {
            $(".message-error").append('Please enter your email to login');
            return false;
        }

        if (password == null || password == '') {
            $(".message-error").append('Password cannot be blank');
            return false;;
        }

        return true;
    }

    function showAjaxLoading() {
        $body.addClass("loading");
    }

    function hideAjaxLoading() {
        setTimeout(() => {
            $body.removeClass("loading");
        }, 250);
    }
});